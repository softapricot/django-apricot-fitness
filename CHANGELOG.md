# Change Log

## [2023.0.5] - 2023-06-28

###Fixed
- Strava measure unit handling

### Added
- Calories converter util
- Activity method to strava integration

## [2023.0.4] - 2023-06-26

### Fixed
- total stats cache

## [2023.0.3] - 2023-06-22

### Added
- `extra_class` parameter to template tags for custom styling

### Fixed
- `StravaIntegration` auth flow failing for app mode

## [2023.0.2] - 2023-06-19

### Added
- Single use integration compatibility
- `AppExampleView` for single use integration compatibility test

### Fixed
- user validation on template tags
- middleware missmatched redirection

## [2023.0.0a1] - 2023-06-15

### Added
- migration of dafitness package from [dapricot](https://gitlab.com/softapricot/django_apricot) 