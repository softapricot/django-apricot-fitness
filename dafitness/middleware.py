""" Middlewares module for Fitness integrations
"""
from django.shortcuts import redirect


class IntegrationsMiddleware:
    """Middleware for API authentication"""

    def __init__(self, get_response):
        self.get_response = get_response
        # One-time configuration and initialization.

    def __call__(self, request):
        params = request.GET

        if "code" in params and "scope" in request.GET:
            next_to = f"{request.session['auth_redirect']}"
            request.session["code"] = request.GET["code"]
            request.session["authenticated"] = True

            return redirect(next_to)

        response = self.get_response(request)
        return response
