""" Views module
"""
from dacommon.integrations.bases import BaseAuthView

from dafitness.integrations.strava import StravaIntegration, Scopes


class StravaAuthView(BaseAuthView):
    """Strava wuthentication view"""

    template_name = "dapricot/fitness/strava_auth.html"
    integration_class = StravaIntegration
    integration_scopes = Scopes
