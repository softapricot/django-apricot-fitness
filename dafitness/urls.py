""" URLs module
"""
from django.urls.conf import path
from dafitness import apps
from dafitness.views import StravaAuthView


app_name = apps.DjangoApricotConfig.label

urlpatterns = [
    path("auth", StravaAuthView.as_view(), name="auth"),
    path("auth/f", StravaAuthView.as_view(), {"force_auth": True}, name="auth_force"),
]
