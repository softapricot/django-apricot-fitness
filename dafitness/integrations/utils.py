""" Integration's utils module
"""
from enum import Enum
from dacommon.utils.converters import BaseConverter


class CaloriesConverter(BaseConverter):
    """Calories converter class"""

    class Unit(Enum):
        """distance units"""

        CAL = 1
        J = 4.187
        KCAL = 1000
        KWH = 860420.6500956

    def get_calories(self):
        """returns value in calories"""
        return self._convert_value_to_unit(self.Unit.CAL)

    def get_kilocalories(self):
        """returns value in kilocalories"""
        return self._convert_value_to_unit(self.Unit.KCAL)

    def get_joules(self):
        """returns value in joules"""
        return self._convert_value_to_unit(self.Unit.J)

    def get_kilowatt_hour(self):
        """returns value in kilowatt hour"""
        return self._convert_value_to_unit(self.Unit.KWH)
