""" Strava integration module
"""
from enum import Enum
from datetime import timedelta, datetime
from urllib.parse import urlencode

from django.shortcuts import redirect
from django.conf import settings

from dacommon.utils.datetime import get_datetime
from dacommon.integrations.bases import BaseIntegration
from dacommon.utils.converters import DistanceConverter, TimeConverter, SpeedConverter

from dafitness.models import DataOrigin, FitnessProfile


class Scopes(Enum):
    """Authorization scopes"""

    ACTIVITY_READ = "activity:read"
    ACTIVITY_READ_ALL = "activity:read_all"
    PROFILE_READ_ALL = "profile:read_all"


class StravaIntegration(BaseIntegration):
    """Integration class for Strava API"""

    origin = DataOrigin.STRAVA
    base_uri = "https://www.strava.com/api/v3/"
    datetime_format = "%Y-%m-%dT%H:%M:%SZ"
    profile_model = FitnessProfile

    def __init__(self, context, scopes: list, user=None):
        self._client_id = settings.STRAVA_CLIENT_ID
        self._client_secret = settings.STRAVA_CLIENT_SECRET
        self.scopes = ",".join(
            scope if isinstance(scope, str) else scope.value for scope in scopes
        )
        super().__init__(context, user)

    def authenticate(self, force=False):
        """Redirection object for integration authentication"""
        url = "https://www.strava.com/oauth/mobile/authorize?" + urlencode(
            {
                "client_id": self._client_id,
                "redirect_uri": f"http://{self._context.get_host()}",
                "response_type": "code",
                "approval_prompt": "force" if force else "auto",
                "scope": self.scopes,
            }
        )

        return redirect(url)

    def generate_auth_token(self, only_refresh: bool = False):
        payload = None
        refresh_token = self.get_token("refresh_token")
        if (
            refresh_token is not None
            and refresh_token.is_expired
            and refresh_token.token is not None
        ):
            payload = {
                "client_id": self._client_id,
                "client_secret": self._client_secret,
                "refresh_token": refresh_token.token,
                "grant_type": "refresh_token",
                "scope": self.scopes,
            }

        elif self._context.session.get("code", None) is not None and not only_refresh:
            payload = {
                "client_id": self._client_id,
                "client_secret": self._client_secret,
                "code": self._context.session["code"],
                "grant_type": "authorization_code",
            }
            self._context.session.pop("code")
            self._context.session.modified = True

        if payload is not None:
            response = self.request(
                "oauth/token",
                StravaIntegration.RequestAction.POST,
                payload=payload,
                verify=False,
            )
            tokens = response.json()

            if self.single_use:
                setattr(self, "access_token", tokens["access_token"])
                setattr(self, "refresh_token", tokens["refresh_token"])

            else:
                self.profile.create_or_update_token(
                    "access_token", tokens["access_token"], tokens["expires_at"]
                )
                self.profile.create_or_update_token(
                    "refresh_token", tokens["refresh_token"], tokens["expires_at"]
                )

    @property
    def athlete(self):
        """Returns athlete data"""
        response = self.request(
            "athlete",
            StravaIntegration.RequestAction.GET,
            auth_headers=self.auth_headers,
            use_cache=True,
        )

        return response.json()

    @property
    def bikes(self):
        """Returns athlete's bikes data"""
        return self.athlete["bikes"]

    def activities(
        self,
        per_page,
        page,
        from_date=None,
        to_date=None,
        cache_expires_after=timedelta(days=1),
    ):
        """Returns user activities"""
        data = {"per_page": per_page, "page": page}
        if from_date is not None:
            data["after"] = get_datetime(from_date, self.datetime_format).timestamp()
        if to_date is not None:
            data["before"] = get_datetime(to_date, self.datetime_format).timestamp()

        response = self.request(
            "athlete/activities",
            StravaIntegration.RequestAction.GET,
            query=data,
            auth_headers=self.auth_headers,
            use_cache=True,
            cache_expires_after=cache_expires_after,
        )
        return response.json()

    def activity(
        self,
        activity_id,
        cache_expires_after=timedelta(days=1),
    ):
        """Returns activity"""
        data = {"include_all_efforts": " "}
        response = self.request(
            f"activities/{activity_id}",
            StravaIntegration.RequestAction.GET,
            query=data,
            auth_headers=self.auth_headers,
            use_cache=True,
            cache_expires_after=cache_expires_after,
        )
        return response.json()

    def total_stats(self, from_date=None, to_date=None):
        """Returns user total stats for specified date range"""
        responses = []
        stats = {
            "distance": 0,
            "moving_time": 0,
            "total_elevation_gain": 0,
            "max_speed": 0,
            "average_speed": 0,
        }
        today = datetime.today()

        for i in range(1, 10):
            response = self.activities(
                per_page=100,
                page=i,
                from_date=today.replace(hour=0, minute=0, second=0)
                if from_date is None
                else from_date,
                to_date=today.replace(hour=23, minute=59, second=59)
                if to_date is None
                else to_date,
                cache_expires_after=timedelta(hours=1),
            )
            if len(response) == 0:
                break

            responses += response

        # TODO: use pandas instead
        for activity in responses:
            stats["distance"] += activity["distance"]
            stats["moving_time"] += activity["moving_time"]
            stats["total_elevation_gain"] += activity["total_elevation_gain"]
            stats["average_speed"] += activity["average_speed"]
            if activity["max_speed"] > stats["max_speed"]:
                stats["max_speed"] = activity["max_speed"]

        stats["average_speed"] = SpeedConverter(
            stats["average_speed"], SpeedConverter.Unit.MTS_S
        ).get_kms_per_hr()
        stats["max_speed"] = round(
            SpeedConverter(
                stats["max_speed"], SpeedConverter.Unit.MTS_S
            ).get_kms_per_hr(),
            2,
        )
        stats["distance"] = round(
            DistanceConverter(
                stats["distance"], DistanceConverter.Unit.MT
            ).get_kilometers(),
            2,
        )
        stats["moving_time"] = TimeConverter(
            stats["moving_time"], TimeConverter.Unit.S
        ).to_string()
        stats["total_elevation_gain"] = round(stats["total_elevation_gain"], 2)

        stats["activities"] = len(responses)
        stats["average_speed"] /= stats["activities"] if stats["activities"] > 0 else 1
        stats["average_speed"] = round(stats["average_speed"], 2)

        from_date = get_datetime(from_date, self.datetime_format).date().isoformat()
        to_date = get_datetime(to_date, self.datetime_format).date().isoformat()
        if from_date == to_date:
            stats["date"] = from_date
        else:
            stats["to_date"] = to_date
            stats["from_date"] = from_date

        return stats
