""" Django Apricot accounts tags module
"""
from django import template
from django.contrib.auth.models import AnonymousUser

from dacommon.utils.datetime import get_datetime, PeriodName, get_period
from dafitness.integrations.strava import StravaIntegration, Scopes

register = template.Library()


@register.inclusion_tag("dapricot/fitness/tags/badge.html", takes_context=True)
def strava_badge(context, user=None):
    """Returns user bikes"""
    request = context["request"]
    data = {
        "api_authenticated": False,
    }
    if user is None:
        user = request.user

    strava = StravaIntegration(request, [Scopes.PROFILE_READ_ALL], user=user)

    if isinstance(user, AnonymousUser) or not strava.is_authenticated:
        return {"api_authenticated": False}

    profile = strava.athlete

    data["profile"] = (
        profile["username"],
        f"https://www.strava.com/athletes/{profile['id']}",
        profile["profile_medium"],
    )
    data["api_authenticated"] = strava.is_authenticated

    return data


@register.inclusion_tag("dapricot/fitness/tags/bikes.html", takes_context=True)
def bikes_cards(context, user=None, extra_class=None):
    """Returns user bikes"""
    request = context["request"]

    if user is None:
        user = request.user

    strava = StravaIntegration(
        request, [Scopes.ACTIVITY_READ_ALL, Scopes.PROFILE_READ_ALL], user=user
    )

    if isinstance(user, AnonymousUser) or not strava.is_authenticated:
        return {"api_authenticated": False}

    bikes = [
        (bike["id"][1:], bike["name"], bike["converted_distance"])
        for bike in strava.bikes
    ]
    return {
        "bikes": bikes,
        "api_authenticated": strava.is_authenticated,
        "extra_class": extra_class,
    }


@register.inclusion_tag("dapricot/fitness/tags/bikes.html", takes_context=True)
def bike_card(context, bike_id, user=None, extra_class=None):
    """Returns user bikes"""
    request = context["request"]

    if user is None:
        user = request.user

    strava = StravaIntegration(
        request, [Scopes.ACTIVITY_READ_ALL, Scopes.PROFILE_READ_ALL], user=user
    )

    if isinstance(user, AnonymousUser) or not strava.is_authenticated:
        return {"api_authenticated": False}

    bikes = [
        (bike["id"][1:], bike["name"], bike["converted_distance"])
        for i, bike in enumerate(strava.bikes)
        if bike_id in (bike["name"], i)
    ]
    return {
        "bikes": bikes,
        "base_style": "secondary",
        "api_authenticated": strava.is_authenticated,
        "extra_class": extra_class,
    }


@register.inclusion_tag("dapricot/fitness/tags/activities.html", takes_context=True)
def activities(context, per_page=5, page=1, user=None, extra_class=None):
    """Returns user activities"""
    request = context["request"]

    if user is None:
        user = request.user

    strava = StravaIntegration(
        request, [Scopes.ACTIVITY_READ_ALL, Scopes.PROFILE_READ_ALL], user=user
    )

    if isinstance(user, AnonymousUser) or not strava.is_authenticated:
        return {"api_authenticated": False}

    items = [
        (
            activity["id"],
            activity["name"],
            activity["type"].lower(),
            f"{round(activity['distance']/1000,1)} km",
            get_datetime(activity["start_date"], strava.datetime_format).date(),
        )
        for activity in strava.activities(per_page, page)
    ]
    return {
        "activities": items,
        "api_authenticated": strava.is_authenticated,
        "extra_class": extra_class,
    }


@register.inclusion_tag("dapricot/fitness/tags/total_stats.html", takes_context=True)
def total_stats(context, from_date=None, to_date=None, user=None, extra_class=None):
    """Returns user activities"""
    request = context["request"]

    if user is None:
        user = request.user

    strava = StravaIntegration(
        request, [Scopes.ACTIVITY_READ_ALL, Scopes.PROFILE_READ_ALL], user=user
    )

    if isinstance(user, AnonymousUser) or not strava.is_authenticated:
        return {"api_authenticated": False}

    # TODO: use some stream object in case of big dates ranges
    stats = strava.total_stats(from_date=from_date, to_date=to_date)

    return {
        "stats": stats,
        "api_authenticated": strava.is_authenticated,
        "extra_class": extra_class,
    }


@register.inclusion_tag("dapricot/fitness/tags/total_stats.html", takes_context=True)
def current_total_stats(context, period=PeriodName.DAY, user=None, extra_class=None):
    """Returns user activities"""

    from_date, to_date = get_period(period_unit=period)
    data = total_stats(context, from_date, to_date, user, extra_class)
    data["period"] = PeriodName(period).value
    return data
