""" Django Apricot Core views module
"""
from dacommon.views import BaseView
from dacommon.integrations.bases import BaseAppView
from dafitness.integrations.strava import StravaIntegration, Scopes


class ExampleView(BaseView):
    """View for example"""

    template_name = "example.html"


class AppExampleView(BaseAppView):
    """View for grid example"""

    template_name = "example_app.html"
    integration_class = StravaIntegration
    integration_scopes = Scopes

    def response_data(self, request):
        data = self.integration.athlete
        self.add_data("strava_info", data["username"], True)
        self.add_data("strava_info", f"{data['firstname']} {data['lastname']}", True)
        self.add_data("strava_info", data["sex"], True)
        self.add_data("strava_info", data["profile"], True)
        self.add_data(
            "strava_info",
            [(bike["name"], bike["converted_distance"]) for bike in data["bikes"]],
            True,
        )
